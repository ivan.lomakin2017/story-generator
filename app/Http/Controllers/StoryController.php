<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StoryController extends Controller
{
	
	public function __construct()
	{
		ini_set('max_execution_time', 300);
	}

	public function show_form()
	{
		return view('generate_form');
	}

	public function generate(Request $request) {
		$title = $request->title;
		$category = $request->category;
		$story = '';
		for ($i = 0; $i < 30; $i++)
			$story .= shell_exec('model.exe ' . $title . ' ' . $category).' ';

		return view('story_result', compact(['title', 'story']));
	}

}
