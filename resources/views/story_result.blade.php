<html>
<head>
  <title>Story Generator</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link
  rel="stylesheet"
  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"
  />
  <style>
    .center-block {
      display: block;
      margin-right: auto;
      margin-left: auto;
    }
  </style>
</head>
<body>
  <div class="container">
    <h1 class="text-center">{{ $title }}</h1>
    <h2>{{ $story }}</h2>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</body>
</html>
