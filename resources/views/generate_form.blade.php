<html>
<head>
  <title>Story Generator</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link
  rel="stylesheet"
  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"
  />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style>
  .center-block {
    display: block;
    margin-right: auto;
    margin-left: auto;
  }
</style>
<body>
  <div class="container center-block">
    <h3 class="text-center">STORY GENERATOR</h3>

    <div class="text-center">
      <form action="/generate" method="POST">
        @csrf
        <div class="form-group">
          <label for="year">Category:</label>
          <input class="form-control" id="category" name="category" placeholder="" required></input>
        </div>
        <div class="form-group">
          <label for="year">Title:</label>
          <input class="form-control" id="title" name="title" placeholder="" required></input>
        </div>
        <br />
        <button type="submit" class="btn btn-primary">Generate</button>
      </form>
    </div>
  </div>
</body>
</html>
